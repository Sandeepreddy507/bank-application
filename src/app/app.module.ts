import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './components/Shared/header/layout.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TransactionComponent } from './components/transcation/transactions.component'
import { BeneficiaryComponent } from './components/benificary/beneficiary.component'
import { FundComponent } from './components/fundtransfer/funds.component';
import { MatTableModule } from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {DataService} from '../app/services/data.service';
import { HttpClientModule } from '@angular/common/http';
import {accountDetails} from './components/accountdetails/accountdetails.component';
import { BankService } from './services/bank.service';
export const routes = [
  {
    path: '', redirectTo: 'login', pathMatch: 'full',
  },

  { path: 'login', component: LoginComponent },
  { path: 'layout', component: LayoutComponent, children: [{ path: '', component: accountDetails }] },
  { path: 'layout/addbeneficiary', component: BeneficiaryComponent },
  { path: 'layout/fundstransfer', component: FundComponent },
  { path: 'layout/transaction', component: TransactionComponent }


]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    TransactionComponent,
    BeneficiaryComponent,
    FundComponent,
    accountDetails,
  
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    MatCardModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatSnackBarModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSelectModule,
     HttpClientModule


  ],
  providers: [
    DataService,
    BankService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
