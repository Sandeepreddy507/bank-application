import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { validateBasis } from '@angular/flex-layout';
import { BankService } from '../../services/bank.service';
import { DataService } from 'src/app/services/data.service';
@Component({
    selector: 'funds',
    templateUrl: './funds.component.html',
})
export class FundComponent {
    funds: FormGroup;
    isSubmitted = false;
    srcId: number;
    destId: number;
    user;
    beneficiaryList: Array<any>;
    minimumBalance: number = 1000;
  
   
    submitted: boolean;

    beneficiaries: Array<any> = [
      {
  
  
      }
    ];
    constructor(private formBuilder: FormBuilder, private router: Router, private bankService: BankService ,private dataService:DataService, private snackbar: MatSnackBar) {

    }
    accountNumbers=[1212121212,12121212,123123123123123]
    ngOnInit() {
        this.funds = this.formBuilder.group({
            sourceAccount: ['', [Validators.required]],
            destinationAccount: ['', [Validators.required]],
            Name: ['', [Validators.required]],
            transferAmount: ['', [Validators.required]]

        })
        this.dataService.getDataByUser(`${'http://localhost:3000'}/BankUsers`, sessionStorage.getItem("email"))
        .subscribe(response => {
  
          this.user = response;
          console.log(response)
          console.log(this.user)
          this.funds.controls.sourceAccount.patchValue(this.user[0].accountNumber)
        //   this.model.srcAccountNumber = this.user[0].accountNumber;
          this.beneficiaryList = this.user[0].beneficiaries;
          for (let i = 0; i < this.beneficiaryList.length; i++) {
            this.bankService.getDataByUser(`${'http://localhost:3000'}/benficaryto`, this.beneficiaryList[i])
              .subscribe(response => {
                  console.log(response)
               // let firstName = response[0].firstName;
                let accountNumber = response[0].accountNumber;
                let resp = response;
                this.beneficiaries.push(response[0]);
                console.log(this.beneficiaries)
              },
                (error) => {
  
                },
                () => {
  
                });
  
          }
        },
          (error) => {
          },
          () => {
          });
    }
    // onTransfer() {
    //     this.isSubmitted = true;
    //     if (this.funds.status == 'INVALID') {
    //         this.snackbar.open('Please fill the all mandatory fields', '', {
    //             duration: 2000,
    //         })
    //     } else {
           
    //     }
    // }
    onTransfer = (transfer, destAccount) => {
        this.isSubmitted = true;
        if (this.funds.controls.transferAmount.value < 1) {
          alert("Minimum Rs.1 is required to transfer");
          return;
        }
        // if (transfer.valid) {
          const srcAccountNumber = this.funds.controls.sourceAccount.value;
          const destinationAccount = destAccount;
          const amount = transfer;
          this.transferFund(this.user[0], destinationAccount, amount);
        // }
      }
    
      /**
       * @description When transfer fund button clicked, this method validates user login
       */
      transferFund = (srcAccount, destAccount, amount) => {
    
        if (srcAccount.currentBalance - amount < this.minimumBalance) {
          alert(`Dear Customer, You need to maintain a minimum balance:Rs.${this.minimumBalance}`);
          return;
        }
        if (srcAccount.currentBalance < amount) {
          alert(`You don't have sufficient balance to make a transfer. You have only Rs.${srcAccount.currentBalance} in your account.`);
          return;
        }
        else {
          let srcUpdatedBalance = srcAccount.currentBalance - amount;
          srcAccount.currentBalance = srcUpdatedBalance;
          this.srcId = srcAccount.id;
    
          let destUpdatedBalance = destAccount + amount;
          destAccount = destUpdatedBalance;
          this.destId = destAccount;
        }
    
        this.dataService.updateData(`${'http://localhost:3000'}/BankUsers/${this.srcId}`, srcAccount)
          .subscribe(response => {
            alert("Fund transferred successfully");
            this.router.navigate(['layout'])
            if (response !== null) {
              this.dataService.updateData(`${'http://localhost:3000'}/benficaryto/${this.destId}`, destAccount)
                .subscribe(response => {
                   alert("Fund transferred successfully");
                   this.router.navigate(['layout'])
     if (response != null) {
                    const postObj = { srcAccountNumber: srcAccount.accountNumber, destAccountNumber: destAccount, transferAmount: amount, tranferDate: new Date() };
                    this.dataService.addData(`${'http://localhost:3000'}/transcations`, postObj)
                      .subscribe(response => {
                        if (response != null) {

                          alert("Fund transferred successfully");
                          this.router.navigate(['layout'])
                        }
                        
                      },
                        (error) => {
    
                        },
                        () => {
    
                        })
                  }
                },
                  (error) => {
    
                  },
                  () => {
    
                  })
            }
    
          },
            (error) => {
    
            },
            () => {
    
            })
    
    
    
      }

}