
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataService } from 'src/app/services/data.service'
import { Subscription } from 'rxjs/internal/Subscription';
@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    login: FormGroup;
    isSubmitted = false;
    subscription: Subscription;
    baseUrl: 'http://localhost:3000/BankUsers'
    constructor(private formBuilder: FormBuilder, private router: Router, private snackbar: MatSnackBar, private dataService: DataService) {

    }
    ngOnInit() {
        this.login = this.formBuilder.group({
            password: ['', [Validators.required]],
            username: ['', [Validators.required]],
        })
    }
    onLogin = () => {
        this.isSubmitted = true;
            this.loginUser();

        
    }
    loginUser = () => {
        if (this.login.status == 'INVALID') {
            this.snackbar.open('Please fill the all mandatory fields', '', {
                duration: 2000,
            })
        }
        else {
            console.log(this.baseUrl)
            this.subscription = this.dataService.validateLogin('http://localhost:3000/BankUsers', this.login.value.username, this.login.value.password)
                .subscribe((response: Array<any>) => {
                    sessionStorage.setItem("email",this.login.value.username);
                    let responseArrayLength = response.length;
                    if (responseArrayLength === 1) {
                        this.router.navigate(['/layout']);
                    }
                    else {
                        alert("Invalid Credentials!");
                    }
                },
                    (error) => {

                    },
                    () => {

                    })
        }
    }
}