import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataService } from '../../services/data.service'
@Component({
    selector: 'transaction',
    templateUrl: './transactions.component.html',
})
export class TransactionComponent {
    displayedColumns: string[] = ['Account', 'Name', 'Amount', 'Type']
    dataSource ;
    constructor(private router: Router, private snackbar: MatSnackBar, private dataService:DataService){
     this.getTransaction()       

    }
    
    addBeneficary() {
        console.log("wegwgeeg")
        this.router.navigate(['layout/addbeneficiary'])
       
      }
      getTransaction(){
        this.dataService.getDataByUser('http://localhost:3000/transcations',sessionStorage.getItem('email')).subscribe(response=> {
           this.dataSource = response
            console.log(this.dataSource)
          },
            (error) => {
            },
            () => {
      
            })
      }
}