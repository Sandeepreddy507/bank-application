import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BankService } from '../../services/bank.service';
import { DataService } from 'src/app/services/data.service';
@Component({
  selector: 'beneficiary',
  templateUrl: './beneficiary.component.html',
})
export class BeneficiaryComponent {
    beneficiary: FormGroup;
    isSubmitted = false;
    user :any ;
  baseUrl:string;
  userAccountNumber:number;
    constructor(private formBuilder: FormBuilder, private router: Router, private snackbar: MatSnackBar, private bankService: BankService ,private dataService:DataService){

    }
    ngOnInit(){
        this.beneficiary = this.formBuilder.group({
            Beneficiary: ['', [Validators.required]],
            Name: ['', [Validators.required]],
            BankName: ['', [Validators.required]],
           
          })
          this.dataService.getDataByUser(`${'http://localhost:3000'}/BankUsers`,sessionStorage.getItem("email"))
          .subscribe(response=>{
             this.user = response;
             this.userAccountNumber =  this.user[0].accountNumber
          },
          (error)=>{
       
          },
          ()=>{
      
          });
    }
//    onsubmit(){
//     this.isSubmitted = true;
//     if (this.beneficiary.status == 'INVALID') {
//         this.snackbar.open('Please fill the all mandatory fields', '', {
//             duration: 2000,
//         })
//     } else {
       
//     }

//    }
onsubmit=(accountNumber)=>
{
  this.isSubmitted = true;

     this.registerBeneficiary(accountNumber);
  
}

/**
* @description This method registers a customer as beneficiary
* @param accountNumber
*/
registerBeneficiary=(accountNumber)=>
{
  this.bankService.findCustomerByAccountnumber(`${'http://localhost:3000/benficaryto'}`,accountNumber).subscribe(response=>{
    let output = response;
    if(output[0]===undefined)
    {
      alert("Given Account Number doesn't exist to add as a beneficiary");
      return;
    } 
    else{
      let loggedInUser = this.user;
      loggedInUser[0].beneficiaries.push(accountNumber);

      this.bankService.addBeneficiary(`${'http://localhost:3000/benficaryto'}/${this.user[0].id}`,loggedInUser[0]).subscribe((response)=>{ 
      alert("Beneficiary added successfully");
      this.router.navigate(['layout'])
      },
      (error)=>{
      },
      ()=>{
  
      })
    }
  },
  (error)=>{
  },
  ()=>{

  })
}
   onCancel(){
       this.router.navigate(['layout'])
   }
  
}